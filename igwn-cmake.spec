#========================================================================
#========================================================================
%define name 	igwn-cmake
%define version 1.2.1
%define release 2.1
%define summary This is a collection of CMake functions used as a replacement for autoconf macros
%if %{?cmake3:1}%{!?cmake3:0}
%define cmake cmake3
%define ctest ctest3
%endif

#========================================================================
# Main spec file
#========================================================================
Name: 		    %{name}
Summary: 	    %{summary}
Version: 	    %{version}
Release: 	    %{release}%{?dist}
License: 	    GPLv2+
Group: 		    LSC Software/Data Analysis
Source: 	    https://software.igwn.org/lscsoft/source/%{name}-%{version}.tar.gz
Packager:       Edward Maros (ed.maroso@ligo.org)
URL: 		    https://git.ligo.org/ldastools/igwn-cmake
BuildArch:      noarch
BuildRoot:      %{buildroot}
BuildRequires:  rpm-build
BuildRequires:  python3-rpm-macros
BuildRequires:  make
BuildRequires:	pkgconfig
BuildRequires:  doxygen
%if 0%{?rhel} > 7
BuildRequires:  cmake >= 3.6
Requires:       cmake >= 3.6
Requires:       python2-beautifulsoup4
%else
BuildRequires:  cmake3 >= 3.6
BuildRequires:  cmake
Requires:       cmake3 >= 3.6
Requires:       cmake
Requires:       python-beautifulsoup4
%endif
Prefix:		    %_prefix

%description


#------------------------------------------------------------------------
# Get onto the fun of building the NDS software
#------------------------------------------------------------------------

%prep
%setup -q

%build
%cmake \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DCMAKE_INSTALL_LIBDIR=%{_libdir} \
    -Wno-dev \
    .

%install
make DESTDIR=%{buildroot} install

%check
%ctest -V %{?_smp_mflags}


#----------------------------------------------
# Do the noarch files
#----------------------------------------------

%files
%_datadir/

%changelog
* Tue Jun 26 2020 Adam Mercer <adam.mercer@ligo.org> 1.2.1-2.1
- update packaging to support el7 and el8

* Thu May 28 2020 Edward Maros <ed.maros@ligo.org> - 1.2.1-1
- Removed .git* files from source distribution

* Wed May 27 2020 Edward Maros <ed.maros@ligo.org> - 1.2.0-1
- Built for new release
